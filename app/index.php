<?php include_once('_header.php') ?>
<div class="card">
    <div class="card-body">
        <div class="bd-example">
            <a type="button" class="btn btn-primary mb-3" href="add.php">Añadir apuntes</a>
            <form action="list.php" method="GET" enctype="multipart/form-data">
                <select name="subject" class="form-control">
                    <option value="mates">Mates</option>
                    <option value="lengua">Lengua</option>
                </select>
                <div class="col-auto mt-3">
                    <button type="submit" class="btn btn-primary">Ver Apuntes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once('_footer.php') ?>